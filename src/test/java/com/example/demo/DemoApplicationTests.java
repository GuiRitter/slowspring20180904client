package com.example.demo;

import static io.restassured.RestAssured.get;

import java.time.LocalDateTime;
import java.util.LinkedList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	public static final String HOST = "127.0.0.1";

	public static final String PORT = "8080";

	public static final String units(double value) {
		return String.format("%s ns = %s μs = %s ms = %s s", value, value / 1000d, value / 1000000d, value / 1000000000d);
	}

	@Test
	public void contextLoads() {}

	@Test
	public void testGet() {
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime stopTime = now.plusMinutes(1);
		long rttTime0, rttTime, rttMin = Long.MAX_VALUE, rttMax = Long.MIN_VALUE;
		long queryTime, queryMin = Long.MAX_VALUE, queryMax = Long.MIN_VALUE;
		LinkedList<Long> rttTimeList = new LinkedList<>();
		LinkedList<Long> queryTimeList = new LinkedList<>();
		Response response;
		while (now.isBefore(stopTime)) {
			rttTime0 = System.nanoTime();
			response = get("http://" + HOST + ":" + PORT + "/slow_spring_2018_09_04/scaleTest/fromRepository");
			rttTime = System.nanoTime() - rttTime0;
			rttMin = Long.min(rttTime, rttMin);
			rttMax = Long.max(rttTime, rttMax);
			rttTimeList.add(rttTime);
			queryTime = Long.parseLong(response.asString());
			queryMin = Long.min(queryTime, queryMin);
			queryMax = Long.max(queryTime, queryMax);
			queryTimeList.add(queryTime);
			System.out.format("query time:\nmin: %s\navg: %s\nmax: %s\nrequest<->response time:\nmin: %s\navg: %s\nmax: %s\n\n",
					units(queryMin),
					units((queryTimeList.stream().skip(queryTimeList.size() / 2).reduce((a, b) -> a + b).get().longValue()) / ((double) queryTimeList.size() / 2)),
					units(queryMax),
					units(rttMin),
					units((rttTimeList.stream().skip(rttTimeList.size() / 2).reduce((a, b) -> a + b).get().longValue()) / ((double) rttTimeList.size() / 2)),
					units(rttMax));
			now = LocalDateTime.now();
		}
	}
}
