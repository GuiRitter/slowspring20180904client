Project created to try to find out why select queries are so slow on Spring with Hibernate and PostgreSQL. Follow the instructions to reproduce.

1. Make sure you have followed the instructions on the server project.
2. Edit `src/test/java/com/example/demo/DemoApplicationTests.java` and change `HOST` (where the server is running) and `PORT` (Tomcat's port) to point to the server.
3. Run `mvn test` inside the project's folder to run the tests.
